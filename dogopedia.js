// Giorgio Timothy Suharianto
// JavaScript 310B - Final Project

(function() {
  window.addEventListener("load", initial);

  function initial() {
    const dogBreedForm = document.getElementById("dog-breed-form");
    document
    dogBreedForm.addEventListener("submit", getDogImages);
  }

  function getDogImages(e) {
    e.preventDefault();
    const inputBar = document.getElementById("dog-breed-input");
    let dogBreedInput = inputBar.value;
    let correctedString = getValidStringFormatForLink(dogBreedInput);
    let fetchLink = `https://dog.ceo/api/breed/${correctedString}/images`;
    fetch(fetchLink)
      .then(function(data) {
        return data.json();
      })
      .then(function(respJson) {
        displayPics(respJson);
      })
      .catch(function() {
        let resultDiv = document.getElementById("image-container");
        let messagePTag = document.createElement('p');
        messagePTag.textContent = "An error has occurred! Be sure to enter a valid dog breed or try again later.";
        resultDiv.appendChild(messagePTag);
      });
  }

  function displayPics(returnedJson) {
    const input = document.getElementById("dog-breed-input");
    let dogBreedInput = input.value;
    localStorage.setItem(dogBreedInput, returnedJson);
    let imgLinkArr = returnedJson.message;
    let index = 0;
    let imageContainer = document.getElementById("image-container");
    let pictureSlides = setInterval(function() {
      if (index < imgLinkArr.length) {
        imageContainer.innerHTML = "";
        let thisImg = document.createElement('img');
        thisImg.src = imgLinkArr[index];
        imageContainer.appendChild(thisImg);
        index++;
      } else {
        clearInterval(pictureSlides);
      }
    }, 3500);
    document
  }

  function getValidStringFormatForLink(userInput) {
    let allLowerCase = userInput.toLowerCase();
    let finalString = allLowerCase.replace(" ", "/");
    return finalString;
  }
})();
